This is a Maven Java Project.

You will need add some user info in local environment variables before running.  I am running on a mac. 

export TEST_USERNAME=""

export TEST_PASSWORD=""

Once Maven is installed then run "mvn clean verify" in the root of the project directory to run the tests.

TestNG is being used as the Runner.

Requirements where to test three things:
    
    1.  Creating a new Jira

    2.  Searching for a Jira

    3.  Updating an existing Jira

There are two tests that cover these requirements:
    
    1. Create a new Jira, Search for the Jira, and verify Summary, Description, and that it is in an Open state
    
    2. Search for an existing Jira and update the Description of Jira

The code broken up as follows:
src/main/

java/

  Driver/BaseDriver This is where we are managing the Firefox driver.  It has minimal in it right now but I have found that having the driver implementation separate allows for us to edit it easier.

  PageActions/

    GeneralPageActions  This handles general actions for anything non-login associated.  Can easily break into more actions as needed

    LoginPageActions   This handles the actions for the login page.

  PageSelectors/

    GeneralPageSelectors  This has all of the X-Paths that we are using in the actions.

  Tests/

    BaseTest   The Base test can hold setup as needed for other tests to extend from.
  
    JiraSubmissionTests  This has the two tests that were mentioned earlier

resources/test.xml  TestNG config file


Actions are the Meat of the framework.  They don't contain Assertions but allow for the test writer to write tests easier.

Notes!!!

    Didn't add any logging.

    Didn't add any Firefox Configuration

    There were a couple of random pop-ups that I saw once or twice that killed the tests but I didn't deal with them here

    I'm using hard waits a whole bunch in here :-(.  This would be better implemented with a periodic CHECK FOR X.

    I'm not checking the jira project when creating the jira's, just creating them under the default project of TST.

    An additional step would be to abstract out the interaction objects.  Creating generic (ButtonObject, LinkObject, TextObject, ...) Then when we new up a object based on X-Path we could define what type of generic object it was and what actions that object could perform.
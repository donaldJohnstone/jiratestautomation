package Tests;

import PageActions.GeneralPageActions;
import PageActions.LoginPageActions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 5:21 PM
 */
public class JiraSubmissionTests extends BaseTest{

    Long uuid;
    String summary;
    String description;

    @BeforeClass
    public void beforeClass() {

        System.currentTimeMillis();

        summary = "An Automated Test " + System.currentTimeMillis();
        description = "The description of a test";

        // Needs to be entered.  Can move this to a Config file that is read
        String username = System.getenv("TEST_USERNAME");
        String password = System.getenv("TEST_PASSWORD");

        LoginPageActions.loginAs(username, password);

        // In case the user isn't taken to the correct test project page
        goToHomePage();
        GeneralPageActions.createNewJiraBug(summary , description);
    }

    @AfterClass
    public void AfterClass(){
        GeneralPageActions.closeIssue(summary);
    }


    /**
     * This test searches for a newly created Jira and validates that it was OPENED with the requested Summary and
     * Description
     */
    @Test(enabled = true)
    public void submitNewJiraBugTest() {

        GeneralPageActions.searchBySummary(summary);
        Assert.assertEquals(GeneralPageActions.getExistingJiraSummary(), summary);
        Assert.assertEquals(GeneralPageActions.getExistingJiraStatus(), "OPEN");
        Assert.assertEquals(GeneralPageActions.getExistingJiraDescription(), description);
    }

    /**
     * This test will search for the Jira that was created in the first test and update the Description then
     * validate that the description was updated.
     */
    @Test(enabled = true, dependsOnMethods = "submitNewJiraBugTest")
    public void updateExistingJiraTest() {

        String newDescription = "This is the new Description";

        GeneralPageActions.searchBySummary(summary);

        GeneralPageActions.updateExistingJiraDescription(newDescription);

        // Some issue with Searching directly after updating the jira.  Going back to homepage for before doing the search
        goToHomePage();

        GeneralPageActions.searchBySummary(summary);

        Assert.assertEquals(GeneralPageActions.getExistingJiraSummary(), summary);
        Assert.assertEquals(GeneralPageActions.getExistingJiraStatus(), "OPEN");
        Assert.assertEquals(GeneralPageActions.getExistingJiraDescription(), newDescription);

    }
}

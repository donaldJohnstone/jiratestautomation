package Tests;

import Driver.BaseDriver;
import org.openqa.selenium.WebDriver;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 5:23 PM
 */
public class BaseTest {

    String homePage = "https://jira.atlassian.com/browse/TST/";
    WebDriver driver = BaseDriver.INSTANCE.getDriver();

    public BaseTest() {
       // This could be gotten from a config file
       goToHomePage();
    }

    public void goToHomePage() {
        driver.get(homePage);
    }

    private static void wait(Long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            System.out.print(e.toString());
        }
    }


}

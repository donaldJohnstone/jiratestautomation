package Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 5:55 PM
 */
public enum BaseDriver {
    INSTANCE;

    private WebDriver driver;

    public WebDriver getDriver() {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
        return driver;
    }

}

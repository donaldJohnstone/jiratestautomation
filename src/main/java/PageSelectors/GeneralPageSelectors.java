package PageSelectors;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 5:45 PM
 */
public class GeneralPageSelectors {

    public static final String GOTO_LOGIN_PAGE_BUTTON = ".//*[@id='user-options']/a[text()=\"Log In\"]";
    public static final String CREATE_JIRA = ".//*[@id='create_link']";
    public static final String SEARCH = ".//*[@id='quickSearchInput']";

    public class createIssueObjects {
        public static final String ISSUE_TYPE_LIST = ".//*[@id='issuetype-field']";
        public static final String BUG_ISSUE_TYPE = ".//*[@id='issuetype-suggestions']//*/a[text()=\"Bug\"]";
        public static final String SUMMARY_TEXT_FIELD = ".//*[@id='summary']";
        public static final String DESCRIPTION_TEXT_FIELD = ".//*[@id='description']";
        public static final String SUBMIT_JIRA_BUTTON = ".//*[@id='create-issue-submit']";
    }

    public class existingIssueObjects {
        public static final String SUMMARY = ".//*[@id='summary-val']";
        public static final String STATUS = ".//*[@id='status-val']/span";
        public static final String DESCRIPTION = ".//*[@id='description-val']/div/p";
        public static final String EDITABLE_DESCRIPTION = ".//*[@id='description']";
        public static final String DESCRIPTION_SAVE = ".//*[@id='description-form']/div[2]/button[1]";
        public static final String CLOSE_ISSUE_REQUEST = ".//*[@id='action_id_2']";
        public static final String REALLY_CLOSE_ISSUE_BUTTON = ".//*[@id='issue-workflow-transition-submit']";
    }

    public class loginObjects {
        public static final String USER_NAME_TEXT_FIELD = ".//*[@id='username']";
        public static final String PASSWORD_TEXT_FIELD = ".//*[@id='password']";
        public static final String SUBMIT_LOGIN_BUTTON = ".//*[@id='loginbutton']";
        public static final String REMEMBER_ME_CHECKBOX = ".//*[@id='rememberme']";
    }

}

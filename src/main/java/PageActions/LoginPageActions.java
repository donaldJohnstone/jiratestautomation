package PageActions;

import Driver.BaseDriver;
import PageSelectors.GeneralPageSelectors;
import PageSelectors.GeneralPageSelectors.loginObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 7:38 PM
 */
public class LoginPageActions {

    public static void loginAs(String username, String password) {
        goToLoginPage();
        enterUserName(username);
        enterPassword(password);
        doNotRememberCreds();
        submitLogin();
    }

    private static void doNotRememberCreds() {
        WebElement rememberMe = BaseDriver.INSTANCE.getDriver().findElement(By.xpath(loginObjects.REMEMBER_ME_CHECKBOX));
        if (rememberMe.isSelected()) {
            rememberMe.click();
        }
    }

    private static void submitLogin() {
        BaseDriver.INSTANCE.getDriver().findElement(By.xpath(loginObjects.SUBMIT_LOGIN_BUTTON)).click();
    }

    private static void goToLoginPage() {
        BaseDriver.INSTANCE.getDriver().findElement(By.xpath(GeneralPageSelectors.GOTO_LOGIN_PAGE_BUTTON)).click();
    }

    private static void enterUserName(String username) {
        BaseDriver.INSTANCE.getDriver().findElement(By.xpath(loginObjects.USER_NAME_TEXT_FIELD)).sendKeys(username);
    }

    private static void enterPassword(String password) {
        BaseDriver.INSTANCE.getDriver().findElement(By.xpath(loginObjects.PASSWORD_TEXT_FIELD)).sendKeys(password);
    }
}

package PageActions;

import Driver.BaseDriver;
import PageSelectors.GeneralPageSelectors;
import PageSelectors.GeneralPageSelectors.createIssueObjects;
import PageSelectors.GeneralPageSelectors.existingIssueObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * User: donald.johnstone
 * Date: 6/5/14
 * Time: 5:46 PM
 */
public class GeneralPageActions {
    
    static WebDriver driver = BaseDriver.INSTANCE.getDriver();

    public enum jiraIssueTypes {
        BUG,
        IMPROVEMENT
    }

    public static void createNewJiraBug(String summary, String description) {
        clickOnCreateNewJira();
        createNewJira(jiraIssueTypes.BUG);
        inputSummary(summary);
        inputDescription(description);
        clickOnSubmitNewJira();
    }

    public static void searchBySummary(String summary) {
        WebElement search = driver.findElement(By.xpath(GeneralPageSelectors.SEARCH));
        search.sendKeys(summary);
        search.sendKeys(Keys.RETURN);
        wait(4000);
    }

    public static String getExistingJiraSummary() {
        return driver.findElement(By.xpath(existingIssueObjects.SUMMARY)).getText();
    }

    public static String getExistingJiraStatus() {
        return driver.findElement(By.xpath(existingIssueObjects.STATUS)).getText();
    }

    public static String getExistingJiraDescription() {
        return driver.findElement(By.xpath(existingIssueObjects.DESCRIPTION)).getText();
    }

    public static void updateExistingJiraDescription(String update) {
        WebElement existingDescription = driver.findElement(By.xpath(existingIssueObjects.DESCRIPTION));

        existingDescription.click();
        wait(1000);

        WebElement editableDescription = driver.findElement(By.xpath(existingIssueObjects.EDITABLE_DESCRIPTION));

        // Clear out the existing string
        editableDescription.clear();

        editableDescription.sendKeys(update);

        driver.findElement(By.xpath(existingIssueObjects.DESCRIPTION_SAVE)).click();
        wait(2000);

    }

    public static void closeIssue(String summary) {
        searchBySummary(summary);
        driver.findElement(By.xpath(existingIssueObjects.CLOSE_ISSUE_REQUEST)).click();
        wait(3000);
        driver.findElement(By.xpath(existingIssueObjects.REALLY_CLOSE_ISSUE_BUTTON)).click();
    }

    private static void inputSummary(String summary) {
        wait(500);
        driver.findElement(By.xpath(createIssueObjects.SUMMARY_TEXT_FIELD)).sendKeys(summary);
    }

    private static void inputDescription(String description) {
        driver.findElement(By.xpath(createIssueObjects.DESCRIPTION_TEXT_FIELD)).sendKeys(description);
    }

    private static void clickOnCreateNewJira() {
        driver.findElement(By.xpath(GeneralPageSelectors.CREATE_JIRA)).click();
    }

    private static void clickOnSubmitNewJira() {
        driver.findElement(By.xpath(createIssueObjects.SUBMIT_JIRA_BUTTON)).click();
        wait(10000);
    }

    private static void wait(int time){
        // This would be better handled as a WAIT UNTIL X EXISTS!!!
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            System.out.print(e.toString());
        }
    }

    private static void createNewJira(jiraIssueTypes issueType) {
        switch (issueType) {
            case BUG: {
                selectBug();
            }
            case IMPROVEMENT: {
                //NOT IMPLEMENTED
            }
        }
    }

    private static void selectBug() {
        // This would be better as a dynamic waiter instead of a hard sleep
        wait(1000);
        driver.findElement(By.xpath(createIssueObjects.ISSUE_TYPE_LIST)).click();
        driver.findElement(By.xpath(createIssueObjects.BUG_ISSUE_TYPE)).click();
    }

}
